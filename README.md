# Trellis / Bedrock / Sage 

## Setup Steps

### Trellis and Bedrock local setup
1. Create new project directory
    `$ mkdir example.com && cd example.com`
2. Clone Trellis
`$ git clone --depth=1 git@github.com:roots/trellis.git && rm -rf trellis/.git`
3. Clone Bedrock
`$ git clone --depth=1 git@github.com:roots/bedrock.git site && rm -rf site/.git`
4. There are two components and places to configure sites:
- Normal settings in `group_vars/development/wordpress_sites.yml`
- asswords/secrets in `group_vars/development/vault.yml`
### Normal Settings  
```
# group_vars/development/wordpress_sites.yml
wordpress_sites:
  example.com:
    site_hosts:
      - canonical: example.test
    local_path: ../site # path targeting local Bedrock site directory (relative to Ansible root)
    admin_email: admin@example.test
    multisite:
      enabled: false
    ssl:
      enabled: false
    cache:
      enabled: false
```
### Passwords/secrets
```
#  group_vars/development/vault.yml
vault_wordpress_sites:
  example.com:
    admin_password: admin
    env:
      db_password: example_dbpassword
```

[More References](https://roots.io/trellis/docs/wordpress-sites/)

## Local Development Setup
Development is handled by Vagrant in Trellis. Our Vagrantfile automatically uses the Ansible provisioner to run the dev.yml playbook and you'll get a virtual machine running your WordPress site.
1. Configure your site(s) based on the WordPress Sites docs and read the development specific ones.
2. Make sure you've edited both `group_vars/development/wordpress_sites.yml` and `group_vars/development/vault.yml`.
3. Optionally configure the IP address at the top of the `vagrant.default.yml` to allow for multiple boxes to be run concurrently (default is `192.168.50.5`).
4. Run `vagrant up` (from your trellis directory, usually the `trellis/ `subdirectory of your project).

Then let Vagrant and Ansible do their thing. After roughly 5-10 minutes you'll have a server running and a WordPress site automatically installed and configured.

To access the VM, run `vagrant ssh`. Sites can be found at `/srv/www/<site name>`. See the [Vagrant docs](https://www.vagrantup.com/docs/cli/) for more commands.

Note that each WP site you configured is synced between your local machine (the host) and the Vagrant VM. Any changes made to your host will be synced to the VM.

Composer and WP-CLI commands need to be run on the virtual machine for any post-provision modifications. Front-end build tools should be run from your host machine and not the Vagrant VM.

Mounting an encrypted folder is not possible with Trellis due to an issue with NFS.

Windows users have a slightly different workflow. See the [Windows docs](https://roots.io/trellis/docs/windows/). 

### Sage Setup
- `cd site/web/wp/wp-content/`
- `rm -rf themes/twentyfifteen/ && rm -rf themes/twentysixteen/ && rm -rf themes/twentyseventeen/ && rm -rf plugins/akismet/ && rm -rf plugins/hello.php`
- `cd site/ && composer update`
- `cd web/app/themes && composer create-project roots/sage sample-theme`
- `cd sample-theme && yarn && yarn install`

Add required wp plugins into `site/composer.json` e.g.
```
  "require": {
  
    "php": ">=5.6",
    
    "composer/installers": "^1.4",
    
    "vlucas/phpdotenv": "^2.0.1",
    
    "johnpbloch/wordpress": "4.9.8",
    
    "oscarotero/env": "^1.1.0",
    
    "wpackagist-plugin/media-file-renamer": "^4.0.4",
    
    "wpackagist-plugin/better-search-replace": "^1.3.2",
    
    "wpackagist-plugin/what-template-file-am-i-viewing": "^1.2",
    
    "wpackagist-plugin/regenerate-thumbnails": "^3.0.2",
    
    "roots/soil": "^3.7",
    
    "roots/wp-password-bcrypt": "1.0.0",
    
    "roots/wp-config": "1.0.0"
    
  },
  ```

Run `composer install` 

Edit config files `site/.env`, `trellis/group_vars/development/wordpress_sites.yml`, `trellis/group_vars/development/vault.yml` and point your browser to browser [www.sample-theme.test/wp/wp-logn](www.sample-theme.test/wp/wp-logn)